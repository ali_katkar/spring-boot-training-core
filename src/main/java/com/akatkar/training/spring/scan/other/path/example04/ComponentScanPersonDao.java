package com.akatkar.training.spring.scan.other.path.example04;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ComponentScanPersonDao {
	
	@Autowired
	private ComponentScanJdbcConnection componentScanJdbcConnection;

	public ComponentScanJdbcConnection getJdbcConnection() {
		return componentScanJdbcConnection;
	}

	public void setJdbcConnection(ComponentScanJdbcConnection jdbcConnection) {
		this.componentScanJdbcConnection = jdbcConnection;
	}
}
