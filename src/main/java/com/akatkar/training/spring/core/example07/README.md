# Demonstration of XML Configuration

### Add following lines into applicationContext.xml inside <beans> tag

  <bean id="jdbcConnection" class="com.akatkar.training.spring.core.example07.JdbcConnection">
  </bean>

  <bean id="personDao" class="com.akatkar.training.spring.core.example07.PersonDao">
    <property name="jdbcConnection" ref="jdbcConnection"></property>
  </bean>
  
 
 ### create application context from xml file
 > ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml")
 
 
 ### retry with constructor injection