# Demonstration of CDI(Contexts and Dependency Injection), Java EE Style Injection

### Add following dependency into pom.xml
		<dependency>
			<groupId>javax.inject</groupId>
			<artifactId>javax.inject</artifactId>
			<version>1</version>
		</dependency>
