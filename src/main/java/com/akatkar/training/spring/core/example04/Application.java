package com.akatkar.training.spring.core.example04;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.akatkar.training.spring.scan.other.path.example04.ComponentScanPersonDao;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		
		ApplicationContext ctx = SpringApplication.run(Application.class, args);
		
		ComponentScanPersonDao personDao = ctx.getBean(ComponentScanPersonDao.class);
		System.out.println(personDao);
		System.out.println(personDao.getJdbcConnection());
		
//		TODO: Remove PersonDao and use from example03
		PersonDao personDao2 = ctx.getBean(PersonDao.class);
		System.out.println(personDao2);
	}

}
