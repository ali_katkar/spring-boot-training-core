package com.akatkar.training.spring.core.example09;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application {

	// Demonstration of External Configuration
	public static void main(String[] args) {

		ApplicationContext ctx = SpringApplication.run(Application.class, args);
		
		MyConfig myClass = ctx.getBean(MyConfig.class);
		
		System.out.println(myClass.getMail());
		System.out.println(myClass.getDatabase());
			
		AppConfig my2 = ctx.getBean(AppConfig.class);
		
		System.out.println(my2);
	}

}
