package com.akatkar.training.spring.core.example01;

public class Application {

	public static void main(String[] args) {
		
		BubbleSortAlgorithm bubbleSortAlgorithm = new BubbleSortAlgorithm();
//		QuickSortAlgorithm quickSort = new QuickSortAlgorithm();
		
		// TODO: Fix variable name to prevent modifying this line
		BinarySearchImpl binarySearchImpl = new BinarySearchImpl(bubbleSortAlgorithm);
			
		
		int result = binarySearchImpl.search(new int[] {12,23,4,5,6}, 5);
		System.out.println(result);
	}

}
