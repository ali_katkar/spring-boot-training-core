package com.akatkar.training.spring.core.example07;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class Application {

	// Demonstration of XML Configuration
	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);

		try (ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml")) {
			PersonDao personDao = ctx.getBean(PersonDao.class);
			System.out.println(personDao);
			System.out.println(personDao.getJdbcConnection());
		}
	}

}
