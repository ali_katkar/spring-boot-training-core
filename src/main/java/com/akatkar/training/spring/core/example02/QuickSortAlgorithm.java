package com.akatkar.training.spring.core.example02;

public class QuickSortAlgorithm implements SortAlgorithm {
	
	public QuickSortAlgorithm() {
		System.out.println("Quicksort Created");
	}

	public int[] sort(int[] numbers) {
		System.out.println("Quick sorted");
		return numbers;
	}
}
