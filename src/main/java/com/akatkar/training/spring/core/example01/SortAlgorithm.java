package com.akatkar.training.spring.core.example01;

public interface SortAlgorithm {

	int[] sort(int[] numbers);
}
