package com.akatkar.training.spring.core.example01;

public class BinarySearchImpl {
	private SortAlgorithm sortAlgorithm;
	
	public BinarySearchImpl(SortAlgorithm sortAlgorithm) {
		this.sortAlgorithm = sortAlgorithm;
	}
	
	public int search(int[] numbers, int number) {
		
		int[] sorted = sortAlgorithm.sort(numbers);
		
		// seacrh in sorted
		
		return 3;
	}

	public SortAlgorithm getSortAlgorithm() {
		return sortAlgorithm;
	}

	public void setSortAlgorithm(SortAlgorithm sortAlgorithm) {
		this.sortAlgorithm = sortAlgorithm;
	}
	
}
