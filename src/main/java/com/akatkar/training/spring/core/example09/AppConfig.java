package com.akatkar.training.spring.core.example09;


public class AppConfig {

	private String appName;
	private String appDescription;
	private String adminFirstName;
	private String adminLastName;
	private String adminMail;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppDescription() {
		return appDescription;
	}

	public void setAppDescription(String appDescription) {
		this.appDescription = appDescription;
	}

	public String getAdminFirstName() {
		return adminFirstName;
	}

	public void setAdminFirstName(String adminFirstName) {
		this.adminFirstName = adminFirstName;
	}

	public String getAdminLastName() {
		return adminLastName;
	}

	public void setAdminLastName(String adminLastName) {
		this.adminLastName = adminLastName;
	}

	public String getAdminMail() {
		return adminMail;
	}

	public void setAdminMail(String adminMail) {
		this.adminMail = adminMail;
	}

	@Override
	public String toString() {
		return "AppConfig [appName=" + appName + ", appDescription=" + appDescription + ", adminFirstName="
				+ adminFirstName + ", adminLastName=" + adminLastName + ", adminMail=" + adminMail + "]";
	}
	
}
