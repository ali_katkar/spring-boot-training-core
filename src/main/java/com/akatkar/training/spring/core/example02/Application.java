package com.akatkar.training.spring.core.example02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		
		ApplicationContext ctx = SpringApplication.run(Application.class, args);
		
		BinarySearchImpl binarySearchImpl = ctx.getBean(BinarySearchImpl.class);
		
		int result = binarySearchImpl.search(new int[] {12,23,4,5,6}, 5);
		System.out.println(result);
	}

}
