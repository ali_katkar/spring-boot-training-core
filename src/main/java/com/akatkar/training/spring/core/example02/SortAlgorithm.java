package com.akatkar.training.spring.core.example02;

public interface SortAlgorithm {

	int[] sort(int[] numbers);
}
