package com.akatkar.training.spring.core.example05;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application {

	// Demonstration of Life Cycle
	public static void main(String[] args) {
		
		ApplicationContext ctx = SpringApplication.run(Application.class, args);
				
		PersonDao personDao = ctx.getBean(PersonDao.class);
		System.out.println(personDao);
		System.out.println(personDao.getJdbcConnection());
		
		System.out.println(personDao.getCreateDate());
		

		PersonDao personDao2 = ctx.getBean(PersonDao.class);
		System.out.println(personDao2);
		System.out.println(personDao2.getJdbcConnection());
		
		System.out.println(personDao2.getCreateDate());	}

}
