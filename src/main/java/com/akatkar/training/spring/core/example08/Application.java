package com.akatkar.training.spring.core.example08;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application {

	// Demonstration of Annotatated Configuration
	// Do NOT use SpringApplication.run and ApplicationContext which returns
	// Use AnnotationConfigApplicationContext
	public static void main(String[] args) {

		ApplicationContext ctx = null;

		PersonDao personDao = ctx.getBean(PersonDao.class);
		System.out.println(personDao);
		System.out.println(personDao.getJdbcConnection());
	}

}
