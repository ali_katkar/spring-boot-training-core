package com.akatkar.training.spring.core.example07;


//Do NOT use any Annotation
public class PersonDao {

	private JdbcConnection jdbcConnection;
	

//	public PersonDao(JdbcConnection jdbcConnection) {
//		this.jdbcConnection = jdbcConnection;
//	}

	public JdbcConnection getJdbcConnection() {
		return jdbcConnection;
	}

	public void setJdbcConnection(JdbcConnection jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}
}
