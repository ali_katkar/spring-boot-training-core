package com.akatkar.training.spring.core.example06;


//Do NOT use Component
public class PersonDao {

	private JdbcConnection jdbcConnection;
	
	public PersonDao(JdbcConnection jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}

	public JdbcConnection getJdbcConnection() {
		return jdbcConnection;
	}

	public void setJdbcConnection(JdbcConnection jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}
}
