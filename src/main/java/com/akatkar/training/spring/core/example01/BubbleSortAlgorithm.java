package com.akatkar.training.spring.core.example01;

public class BubbleSortAlgorithm implements SortAlgorithm {

	public BubbleSortAlgorithm() {
		System.out.println("BubbleSort Created");
	}

	public int[] sort(int[] numbers) {
		System.out.println("Bubble sorted");
		return numbers;
	}
}
