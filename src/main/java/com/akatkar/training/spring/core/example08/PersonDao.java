package com.akatkar.training.spring.core.example08;

import org.springframework.stereotype.Component;

@Component
public class PersonDao {

	private JdbcConnection jdbcConnection;
	

	public PersonDao(JdbcConnection jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}

	public JdbcConnection getJdbcConnection() {
		return jdbcConnection;
	}

	public void setJdbcConnection(JdbcConnection jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}
}
