package com.akatkar.training.spring.core.example09;


public class MyConfig {

	private String mail;
	
	private String database;

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}
}
