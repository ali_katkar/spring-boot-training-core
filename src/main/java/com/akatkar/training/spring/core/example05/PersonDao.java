package com.akatkar.training.spring.core.example05;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonDao {

	private Date createDate;

	@Autowired
	private JdbcConnection jdbcConnection;

		
	public void postConstruct() {
		System.out.println("PersonDao created");
		createDate = new Date();
	}
	
	public void preDestroy() {
		System.out.println("PersonDao Destroyed");;
	}
	

	public Date getCreateDate() {
		return createDate;
	}

	public JdbcConnection getJdbcConnection() {
		return jdbcConnection;
	}

	public void setJdbcConnection(JdbcConnection jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}

	
}
